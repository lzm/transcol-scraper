#!/bin/sh

cd /home/lzm/transcol-scraper/dist

#../scraper.py

md5sum -c timetable.md5 && exit
md5sum timetable.json > timetable.md5

../bigfield.py > bigfield.txt
../transcol.py > transcol.txt

jar uf Bigfield.jar bigfield.txt
jar uf Transcol.jar transcol.txt

perl -pi -e"s/Size: [0-9]+/Size: `stat -c %s Bigfield.jar`/" Bigfield.jad
perl -pi -e"s/Size: [0-9]+/Size: `stat -c %s Transcol.jar`/" Transcol.jad

zip transcol.zip {Bigfield,Transcol}.{jad,jar}
cp transcol.zip /home/lzm/www/transcol/transcol.zip

echo "New version!" | mutt -a transcol.zip -s "Transcol App" -- $1

