#!/usr/bin/env python

import json, re

cs = {'\xc1':'A', '\xc3':'A', '\xc2':'A', '\xc7':'C', '\xc9':'E', '\xaa':'a.',
  '\xcd':'I', '\xca':'E', '\xd3':'O', '\xd4':'O', '\xda':'U', '\xba':'o.',
  '\xe1':'a', '\xe3':'a', '\xe7':'c', '\xe9':'e', '\xed':'i', '\xf3':'o', 
  '\xb4':'\''}

low = ['VIA', 'DA', 'DE', 'DO', 'E']

upp = ['BR', 'BR101', 'II', 'III', 'UFES']

subs = {
  r' +':             ' ',
  r' ?\/ ?':         '/',
  r'\( ?([^ ]+) ?\)':  r'(\1)',
  r'TERMINAL':       'T.',
  r'T\. ?':          'T. '}

# ---

def fix_char(s):
  return ''.join([cs.get(c, c) for c in s])

def fix(s):
  for pat, sub in subs.iteritems():
    s = re.sub(pat, sub, s)
  s = fix_char(s)

  r = ''
  for w in re.split('(\W+)', s):
    if w in low: r += w.lower()
    elif w in upp: r += w
    else: r += w.capitalize()
  return r

# ---

def run():
  all = json.read(open('timetable.json').read())

  all2 = dict([(x,{'lns':[], 'tbl':[], 'obs':[]}) for x in '156789'])

  asd = set()

  for linha, tbl, obs in all:
    linha2 = fix(linha)

    if linha2[0] == '0':
      linha2 = linha2[1:]

    a = all2[linha2[0]]

    a['lns'].append(linha2)
    obs2 = [fix_char(x).upper() for x in (obs or [])]

    a['obs'].append(obs2)

    tbl2 = [[],[]]

    def tbl_sort(t1, t2):
      if t1[0] > t2[0]:
        return 1
      if t1[0] < t2[0]:
        return -1
      return 0

    tbl.sort(tbl_sort)

    for saida, dias, hrs in tbl:
      saida2 = fix(saida)
      dias2 = fix(dias)
      hrs2 = ' '.join(hrs)

      if saida2 not in tbl2[0]:
        s = [[],[]]
        tbl2[0].append(saida2)
        tbl2[1].append(s)

      s[0].append(dias2)
      s[1].append(hrs2)

    a['tbl'].append(tbl2)

  pnum = []
  p = 0

  for n in '156789':
    all = all2[n]
    pnum.append(str(p))
    n = len(all['lns'])

    print '\n'.join(all['lns'])
    p += n

    for o in all['obs']:
      print len(o)
      if o:
        print '\n'.join(o)
      p += len(o) + 1

    ptbl = []

    for saidas, dias2 in all['tbl']:
      ptbl.append(str(p))

      print len(saidas)
      print '\n'.join(saidas)
      p += len(saidas) + 1

      for dias, hrs in dias2:
        print len(dias)
        print '\n'.join(dias)
        print '\n'.join(hrs)
        p += len(dias)*2 + 1

    print '\n'.join(ptbl)
    print n
    p += n + 1

  print '\n'.join(pnum)
  print p

if __name__ == '__main__':
  run()

