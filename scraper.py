#!/usr/bin/env python

import re, urllib2, json

url = 'http://www.ceturb.es.gov.br/default.asp'
data = 'ItemMenu=15980'
data1 = 'ItemMenu=adm510.asp&PaginaDestino=%s&quallinha=%s'

# ---

def get_lines():
  lns = []

  for ln in urllib2.urlopen(url, data):
    ln = ln.strip()

    m = re.match('.*PaginaDestino" value="(.*)"', ln)
    if m:
      pg = m.group(1).strip()

    m = re.match('<option value="(.*)">(.*)</option>', ln)
    if m:
      l = m.group(2).strip()
      l = re.sub(' +', ' ', l)
      l = re.sub(' ?\/ ?', '-', l)

      lns.append((pg, m.group(1).strip(), l))

  return lns

# ----

def get_table(pg, num):
  html = urllib2.urlopen(url, data1 % (pg, num)).read()
	
  body = html.split('<body>')[1].split('</body>')[0]

  tbl, obs = [], None

  for ln in body[2:].split('\n'):
    ln = ln.strip()

    m = re.match('<br /><hr /><span class="Bold">[^ ]+: (.*</span><hr />)', ln)
    if m:
      saida = m.group(1).split('</span>')[0].strip()

    m = re.match('<br /><span class="Bold">(.*)</span>', ln)
    if m:
      dias = m.group(1).strip()
      hrs = []
      tbl.append((saida, dias, hrs))

    m = re.match('<td style="width.*>(.*:.*)</td>', ln)
    if m:
      h = m.group(1).strip()
      hrs.append(h)

    o = re.match('OBS.*', ln)
    if o:
      obs = []

    m = re.match('(&nbsp;)+(.*)<br />', ln)
    if m and obs != None:
      obs.append(m.group(2))

  return tbl, obs

# ----

def run():
  lns = get_lines()

  all = []

  for pg, num, linha in lns:
    print linha
    try:
      tbl, obs = get_table(pg, num)

      all.append((linha, tbl, obs))

    except urllib2.HTTPError:
      pass

  open('timetable.json', 'w').write(json.write(all))

# ----

if __name__ == '__main__':
  run()
