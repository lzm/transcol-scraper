#!/usr/bin/env python

import json, re

def run():
	all = json.read(open('timetable.json').read())

	lns = {
		'0509':'TERMINAL CAMPO GRANDE',
		'0515':'TERMINAL CAMPO GRANDE',
		'0526':'TERMINAL CAMPO GRANDE',
		'0531':'TERMINAL CAMPO GRANDE',
		#'0535':'TERMINAL CAMPO GRANDE',
		'0591':'TERMINAL CAMPO GRANDE',
		'1900':'MARCILIO DE NORONHA',
		'1901':'MARCILIO DE NORONHA',
		'1902':'MARCILIO DE NORONHA',
		'1903':'VIANA'
	}

	lns2 = []

	for linha, tbl, obs in all:
		num = linha.split()[0]
		if num not in lns:
			continue

		ds = {}
		for saida, dias, horas in tbl:
			if saida == lns[num]:
				ds[dias[3]] = horas

		lns2.append([num, ds, linha, obs])

	lns2.sort()

	for num, ds, linha, obs in lns2:
		print int(num)
		print
		for d in 'SAIP':
			if d in ds:
				print '\t'.join(ds[d])
			else:
				print '-'
			print
		print linha
		if obs:
			print '-'
			print '\n'.join(obs)
		print
	
	print 'end'

if __name__ == '__main__':
	run()

